URLs:
http://localhost:8080 - открыть Swagger UI
http://localhost:8080/h2-console - открыть H2-console UI
------------------------------------------------------------------------------
How use:

Собрать проект можно:
 - Maven(+ имеется флаг для генирации классов по файлу telecom.yaml)
 - Gradle

3 runConfiguration файла для запуска через IntelliJ IDEA
------------------------------------------------------------------------------
3 layers:
- repository(используется Spring Data)
- service
- controller(сгенерирован с помощью Swagger)
------------------------------------------------------------------------------
Data Base:
База данных генерируется из JPA Entity(hibernate.hbm2ddl.auto=update)
+ при старте добавляется 1 стандартный пакет для подключени(если его нет в БД)
Entities:
- id для SimCard = номер телефона(сейчас генерируется рандомно,
но в будущем нужно использовать отедельную таблицу с доступными номерами телефонов)
------------------------------------------------------------------------------
Tasklet:
Каждые 3 минуты выполняется Tasklet, который чистит базу от "ненужных" симкарт и пакетов
Расписание можно настроить в файле resources/config/job-config.xml
------------------------------------------------------------------------------
Controllers:
1.UserApiController:
- addUser() - при создании нового юзера, ему добавляется 1 сим карта с 1 стандартным пакетом
- findAllUsers() - найти всех юзеров
- findUserByPassportNumber() - найти юзера по passportNumber
- removeUserByPassportNumber() - удалить юзера по passportNumber
2.SimCardApiController:
- addSimCardByUserPassportNumber() - добавить 1 сим карту с 1 стандартным пакетом определённому юзеру по passportNumber
- deactivatedSimCardById() - декативировать сим карту по id(будет вычищена из базы Tasklet'ом)
- findActivatedSimCardById() - найти активированную сим карту по id
- findAllActivatedSimCard() - найти все активированые сим карты
3.PackApiController:
- addPackToActivatedSimCard() - добавить один пакет(из существующих пакетов) на определённую сим карту по id сим карты
- findAllAvailableByActivatedSimCardId() - найти все доступные пакеты для активной симкарты по id сим карты
- findAvailablePackById() - найти доступный пакет по id
- updatePackBySimCardId() - обновить баланс(списать Minutes/Bytes) c сим карты по id сим карты
4.ExistPackApiController:
- addExistPack() - добавить новый пакет в набор существующих
- findAllExistPacks() - найти все существующие пакеты, которые можно подключать
- findExistPackById() - найсти существующий пакет(который можно подключить) по id
5.BalanceApiController:
- getCurrentBalanceBySimCardId() - запросить баланс по id сим карты

TODO:
1.Генерировать id симкарты БД + добавить новое поле "phone_number"
2.Использовать LiquiBase для создания и изменения таблиц
3.Заменить Tasklet на SpringBatch job
4.Дописать gradle скрип
