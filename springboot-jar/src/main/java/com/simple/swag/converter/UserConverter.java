package com.simple.swag.converter;

import com.simple.swag.entity.UserEntity;
import com.swagger.model.SNewUser;
import com.swagger.model.SUser;

import java.util.function.Function;

public class UserConverter implements Function<UserEntity, SUser> {

    @Override
    public SUser apply(UserEntity userEntity) {
        SUser sUser = new SUser();
        sUser.setId(userEntity.getId());
        sUser.setPassportNumber(userEntity.getPassportNumber());
        sUser.setEmail(userEntity.getEmail());

        return sUser;
    }

    public static UserEntity unconvert(SNewUser sNewUser){
        UserEntity userEntity = new UserEntity();
        userEntity.setPassportNumber(sNewUser.getPassportNumber());
        userEntity.setEmail(sNewUser.getEmail());

        return userEntity;
    }
}
