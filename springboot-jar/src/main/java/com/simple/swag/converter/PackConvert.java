package com.simple.swag.converter;

import com.simple.swag.entity.PackEntity;
import com.swagger.model.SPack;

import java.util.function.Function;

public class PackConvert implements Function<PackEntity, SPack> {

    @Override
    public SPack apply(PackEntity packEntity) {
        SPack sPack = new SPack();
        sPack.setId(packEntity.getId());
        sPack.setMinutes(packEntity.getMinutes());
        sPack.setBytes(packEntity.getBytes());
        sPack.setDateEnd(packEntity.getDateEnd());
        sPack.setStatus(packEntity.getStatus());
        return sPack;
    }

    public static PackEntity unconvert(SPack sPack){
        PackEntity packEntity = new PackEntity();
        packEntity.setId(sPack.getId());
        packEntity.setMinutes(sPack.getMinutes());
        packEntity.setBytes(sPack.getBytes());
        packEntity.setDateEnd(sPack.getDateEnd());
        packEntity.setStatus(sPack.getStatus());
        return packEntity;
    }
}
