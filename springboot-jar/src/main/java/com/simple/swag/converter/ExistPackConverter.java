package com.simple.swag.converter;

import com.simple.swag.entity.ExistPackEntity;
import com.swagger.model.SExistPack;
import com.swagger.model.SNewExistPack;

import java.util.function.Function;

public class ExistPackConverter implements Function<ExistPackEntity, SExistPack> {

    @Override
    public SExistPack apply(ExistPackEntity existPackEntity) {
        SExistPack sExistPack = new SExistPack();
        sExistPack.setId(existPackEntity.getId());
        sExistPack.setMinutes(existPackEntity.getMinutes());
        sExistPack.setBytes(existPackEntity.getBytes());
        sExistPack.setDayLimit(existPackEntity.getDayLimit());
        return sExistPack;
    }

    public static ExistPackEntity unconvert(SExistPack sExistPack){
        ExistPackEntity existPackEntity = new ExistPackEntity();
        existPackEntity.setMinutes(sExistPack.getMinutes());
        existPackEntity.setBytes(sExistPack.getBytes());
        existPackEntity.setDayLimit(sExistPack.getDayLimit());
        return existPackEntity;
    }
    public static ExistPackEntity unconvertNewExistPack(SNewExistPack sNewExistPack){
        ExistPackEntity existPackEntity = new ExistPackEntity();
        existPackEntity.setMinutes(sNewExistPack.getMinutes());
        existPackEntity.setBytes(sNewExistPack.getBytes());
        existPackEntity.setDayLimit(sNewExistPack.getDayLimit());
        return existPackEntity;
    }
}
