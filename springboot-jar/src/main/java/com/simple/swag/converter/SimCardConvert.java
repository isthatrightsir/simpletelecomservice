package com.simple.swag.converter;

import com.simple.swag.entity.SimCardEntity;
import com.swagger.model.SSimCard;

import java.util.function.Function;

public class SimCardConvert implements Function<SimCardEntity, SSimCard> {

    @Override
    public SSimCard apply(SimCardEntity simCardEntity) {
        SSimCard sSimCard = new SSimCard();
        sSimCard.setId(simCardEntity.getId());
        sSimCard.setStatus(simCardEntity.getStatus());

        return sSimCard;
    }
}
