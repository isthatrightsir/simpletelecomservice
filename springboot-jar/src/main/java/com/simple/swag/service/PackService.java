package com.simple.swag.service;

import com.simple.swag.converter.ExistPackConverter;
import com.simple.swag.converter.PackConvert;
import com.simple.swag.entity.ExistPackEntity;
import com.simple.swag.entity.PackEntity;
import com.simple.swag.entity.SimCardEntity;
import com.simple.swag.enumeration.PackStatus;
import com.simple.swag.exception.ServiceException;
import com.simple.swag.repository.ExistPackRepository;
import com.simple.swag.repository.PackRepository;
import com.simple.swag.repository.SimCardRepository;
import com.simple.swag.util.Util;
import com.swagger.model.SExistPack;
import com.swagger.model.SNewExistPack;
import com.swagger.model.SPack;
import com.swagger.model.TrafficModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.simple.swag.constant.Constants.TIME_DATE_PATTERN;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackForClassName = "ServiceException")
public class PackService {

    private static final Logger log = LoggerFactory.getLogger(PackService.class);

    private PackRepository packRepository;
    private ExistPackRepository existPackRepository;
    private SimCardRepository simCardRepository;

    @Autowired
    public void setPackRepository(PackRepository packRepository) {
        this.packRepository = packRepository;
    }

    @Autowired
    public void setExistPackRepository(ExistPackRepository existPackRepository) {
        this.existPackRepository = existPackRepository;
    }

    @Autowired
    public void setSimCardRepository(SimCardRepository simCardRepository) {
        this.simCardRepository = simCardRepository;
    }

    @Transactional(readOnly = true)
    public Page<SPack> findAllAvailableByActivatedSimCardId(Long simCardId, Integer page, Integer size) throws ServiceException {
        SimCardEntity simCardEntity = simCardRepository.findActivatedById(simCardId);
        if (simCardEntity != null) {
            Pageable pageable = null;
            final Long currentDate = Util.getCurrentDate();
            if (page >= 0 && size > 0) {
                pageable = PageRequest.of(page, size);
            }
            return packRepository.findAllAvailableBySimCardIdOrderByDateEndAsc(simCardId, currentDate, pageable).map(new PackConvert());
        }
        throw new ServiceException("Activated SimCard doesnt exist with id = " + simCardId);
    }

    @Transactional(readOnly = true)
    public SPack findAvailablePackById(Long id) throws ServiceException {
        final Long currentDate = Util.getCurrentDate();
        PackEntity packEntity = packRepository.findAvailableByIdAndStatus(id, currentDate);
        if (packEntity != null) {
            return new PackConvert().apply(packEntity);
        }
        throw new ServiceException("Pack doesnt exist with id = " + id);
    }

    public Long addPackToActivatedSimCard(Long simCardId, Long existPackId) throws ServiceException {
        SimCardEntity simCardEntity = simCardRepository.findActivatedById(simCardId);
        if (simCardEntity != null){
            return addPack(simCardEntity, existPackId);
        }
        throw new ServiceException("Activated SimCard doesnt exist with id = " + simCardId);
    }

    Long addPack(SimCardEntity simCardEntity, Long existPackId) throws ServiceException {
        ExistPackEntity existPackEntity = ExistPackConverter.unconvert(findExistPackById(existPackId));
        PackEntity packEntity = new PackEntity();
        packEntity.setMinutes(existPackEntity.getMinutes());
        packEntity.setBytes(existPackEntity.getBytes());
        packEntity.setStatus(PackStatus.AVAILABLE.name());
        LocalDateTime dt = LocalDateTime.now().plusDays(existPackEntity.getDayLimit()); //plus days
        Long dateEnd = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt));
        packEntity.setDateEnd(dateEnd);
        packEntity.setSimCardEntity(simCardEntity);
        packEntity = packRepository.save(packEntity);
        return packEntity.getId();
    }

    //TODO: improve synchronized
    public synchronized void updatePackBySimCardId(Long simCardId, Integer minutes, Long bytes) throws ServiceException {
        SimCardEntity simCardEntity = simCardRepository.findActivatedById(simCardId);
        if (simCardEntity == null) {
            throw new ServiceException("Activated SimCard doesnt exist with id = " + simCardId);
        }
        Long currentDate = Util.getCurrentDate();
        Page<PackEntity> packEntities = packRepository.findAllAvailableBySimCardIdOrderByDateEndAsc(simCardId, 1L, null);
        List<PackEntity> list = packEntities.getContent();
        int availableMinutes = getBalanceMinutes(list);
        long availableBytes = getBalanceBytes(list);
        if (availableMinutes < minutes) {
            throw new ServiceException("Activated SimCard doesnt have enough minutes. Please check balance SimCard with id = " + simCardId);
        }
        if (availableBytes < bytes) {
            throw new ServiceException("Activated SimCard doesnt have enough bytes. Please check balance SimCard with id = " + simCardId);
        }
        int remainingMinutes = minutes;
        long remainingBytes = bytes;
        for (PackEntity packEntity: list){
            if(remainingMinutes > 0){
                if (packEntity.getMinutes() >= remainingMinutes) {
                    packEntity.setMinutes(packEntity.getMinutes() - remainingMinutes);
                    remainingMinutes = 0;
                } else {
                    remainingMinutes = remainingMinutes - packEntity.getMinutes();
                    packEntity.setMinutes(0);
                }
            }
            if(remainingBytes > 0){
                if (packEntity.getBytes() >= remainingBytes) {
                    packEntity.setBytes(packEntity.getBytes() - remainingBytes);
                    remainingBytes = 0;
                } else {
                    remainingBytes = remainingBytes - packEntity.getMinutes();
                    packEntity.setBytes(0L);
                }
            }
            packRepository.save(packEntity);
            if (remainingMinutes == 0 && remainingBytes == 0){
                break;
            }
        }
    }

    //TODO: better move to Batch jobs from service layer
    public void markExpiredPacksAsUnavailable(){
        final Long currentDate = Util.getCurrentDate();
        List<PackEntity> packEntities = packRepository.findAllUnavailableOrderByIdDesc(currentDate);
        for (PackEntity packEntity: packEntities) {
            packEntity.setStatus(PackStatus.UNAVAILABLE.name());
            packRepository.save(packEntity);
        }
    }
    //TODO: better move to Batch jobs from service layer
    public void removedAllUnavailablePacks(){
        packRepository.deleteByStatus(PackStatus.UNAVAILABLE.name());
        log.info("Deleted all pack with status = UNAVAILABLE");
    }

    @Transactional(readOnly = true)
    public TrafficModel getCurrentBalanceBySimCardId(Long simCardId) throws ServiceException {
        SimCardEntity simCardEntity = simCardRepository.findActivatedById(simCardId);
        if (simCardEntity != null) {
            final Long currentDate = Util.getCurrentDate();
            Page<PackEntity> packEntities = packRepository.findAllAvailableBySimCardId(simCardId, currentDate, null);
            List<PackEntity> list = packEntities.getContent();
            int minutes = getBalanceMinutes(list);
            long bytes = getBalanceBytes(list);
            TrafficModel trafficModel = new TrafficModel();
            trafficModel.setMinutes(minutes);
            trafficModel.setBytes(bytes);
            return trafficModel;
        }
        throw new ServiceException("Activated SimCard doesnt exist with id = " + simCardId);
    }

    private static int getBalanceMinutes(List<PackEntity> list) {
        int minutes = 0;
        for (PackEntity packEntity: list)
            minutes = minutes + packEntity.getMinutes();
        return minutes;
    }

    private long getBalanceBytes(List<PackEntity> list) {
        long bytes = 0;
        for (PackEntity packEntity: list)
            bytes = bytes + packEntity.getBytes();
        return bytes;
    }

    @Transactional(readOnly = true)
    public Page<SExistPack> findAllExistPacks(Integer page, Integer size){
        Pageable pageable = null;
        if (page >= 0 && size > 0) {
            pageable = PageRequest.of(page, size);
        }
        return existPackRepository.findAllByOrderByIdDesc(pageable).map(new ExistPackConverter());
    }

    @Transactional(readOnly = true)
    public SExistPack findExistPackById(long existPackId) throws ServiceException {
        if (existPackRepository.existsById(existPackId)) {
            return new ExistPackConverter().apply(existPackRepository.getOne(existPackId));
        }
        throw new ServiceException("ExistPack doesnt exist with id = " + existPackId);
    }

    public Long addExistPack(SNewExistPack sNewExistPack) throws ServiceException {
        if (!existPackRepository.existsByMinutesAndBytesAndDayLimit(sNewExistPack.getMinutes(), sNewExistPack.getBytes(), sNewExistPack.getDayLimit())){
            ExistPackEntity existPackEntity = existPackRepository.save(ExistPackConverter.unconvertNewExistPack(sNewExistPack));
            log.info("Inserted new pack to existing pack set");
            return existPackEntity.getId();
        }
        throw new ServiceException("This existPack already exist");
    }
}
