package com.simple.swag.service;

import com.simple.swag.converter.UserConverter;
import com.simple.swag.entity.UserEntity;
import com.simple.swag.exception.ServiceException;
import com.simple.swag.repository.UserRepository;
import com.swagger.model.SNewUser;
import com.swagger.model.SUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackForClassName = "ServiceException")
public class UserService {

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    private UserRepository userRepository;
    private SimCardService simCardService;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setSimCardService(SimCardService simCardService) {
        this.simCardService = simCardService;
    }

    @Transactional(readOnly = true)
    public Page<SUser> findAllUsers(Integer page, Integer size){
        Pageable pageable = null;
        if (page >= 0 && size > 0) {
            pageable = PageRequest.of(page, size);
        }
        return userRepository.findAllByOrderByPassportNumberAsc(pageable).map(new UserConverter());
    }

    @Transactional(readOnly = true)
    public SUser findUserByPassportNumber(Long passportNumber) throws ServiceException {
        UserEntity userEntity = userRepository.findByPassportNumber(passportNumber);
        if (userEntity != null) {
            return new UserConverter().apply(userEntity);
        }
        throw new ServiceException("User doesnt exist with passport number = " + passportNumber);
    }

    public Long addNewUser(SNewUser sNewUser) throws ServiceException {
        UserEntity userEntity = UserConverter.unconvert(sNewUser);
        if (!userRepository.existsByPassportNumber(userEntity.getPassportNumber())){
            userEntity = userRepository.save(userEntity);
            simCardService.addSimCard(userEntity);
            return userEntity.getId();
        }
        throw new ServiceException("This user already exist");
    }

    public void removeUserByPassportNumber(Long passportNumber) throws ServiceException {
        UserEntity userEntity = userRepository.findByPassportNumber(passportNumber);
        if (userEntity == null) {
            throw new ServiceException("User doesnt exist with passport number = " + passportNumber);
        }
        userRepository.deleteById(userEntity.getId());
    }
}
