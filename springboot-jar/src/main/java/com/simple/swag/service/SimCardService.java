package com.simple.swag.service;

import com.simple.swag.converter.SimCardConvert;
import com.simple.swag.entity.PackEntity;
import com.simple.swag.entity.SimCardEntity;
import com.simple.swag.entity.UserEntity;
import com.simple.swag.enumeration.PackStatus;
import com.simple.swag.enumeration.SimCardStatus;
import com.simple.swag.exception.ServiceException;
import com.simple.swag.repository.SimCardRepository;
import com.simple.swag.repository.UserRepository;
import com.simple.swag.util.Util;
import com.swagger.model.SSimCard;
import com.swagger.model.TrafficModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.simple.swag.constant.Constants.TIME_DATE_PATTERN;


@Service
@Transactional(isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED, rollbackForClassName = "ServiceException")
public class SimCardService {

    private static final Logger log = LoggerFactory.getLogger(SimCardService.class);

    private UserRepository userRepository;
    private SimCardRepository simCardRepository;
    private PackService packService;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setSimCardRepository(SimCardRepository simCardRepository) {
        this.simCardRepository = simCardRepository;
    }

    @Autowired
    public void setPackService(PackService packService) {
        this.packService = packService;
    }

    @Transactional(readOnly = true)
    public Page<SSimCard> findAllActivatedSimCard(Integer page, Integer size){
        Pageable pageable = null;
        if (page >= 0 && size > 0) {
            pageable = PageRequest.of(page, size);
        }
        return simCardRepository.findAllActivatedByOrderByIdDesc(pageable).map(new SimCardConvert());
    }

    @Transactional(readOnly = true)
    public SSimCard findActivatedSimCardById(Long id) throws ServiceException {
        SimCardEntity simCardEntity = simCardRepository.findActivatedById(id);
        if (simCardEntity != null) {
            return new SimCardConvert().apply(simCardEntity);
        }
        throw new ServiceException("Activated SimCard doesnt exist with id = " + id);
    }

    public Long addSimCardByUserPassportNumber(Long userPassportNumber) throws ServiceException {
        UserEntity userEntity = userRepository.findByPassportNumber(userPassportNumber);
        if (userEntity != null) {
            return addSimCard(userEntity);
        }
        throw new ServiceException("User doesnt exist with passport number = " + userPassportNumber);
    }

    Long addSimCard(UserEntity userEntity) throws ServiceException {
        //TODO: how generate simCard ID(phone number)
        long id = System.currentTimeMillis();
        SimCardEntity simCardEntity = new SimCardEntity();
        simCardEntity.setId(id);
        simCardEntity.setStatus(SimCardStatus.ACTIVATED.name());
        simCardEntity.setUserEntity(userEntity);
        simCardEntity = simCardRepository.save(simCardEntity);
        packService.addPack(simCardEntity, 1L); //add standard pack
        return simCardEntity.getId();
    }

    public void markAsDeactivatedSimCardById(Long id) throws ServiceException {
        SimCardEntity simCardEntity = simCardRepository.findActivatedById(id);
        if (simCardEntity == null) {
            throw new ServiceException("Activated SimCard doesnt exist with id = " + id);
        }
        simCardEntity.setStatus(SimCardStatus.DEACTIVATED.name());
        simCardRepository.save(simCardEntity);
        log.info("Activated SimCards is deactivated with id = " + simCardEntity.getId());
    }

    //TODO: better move to Batch jobs from service layer
    public void removeAllDeactivatedSimCards() {
        simCardRepository.deleteByStatus(SimCardStatus.DEACTIVATED.name());
        log.info("Deleted all deactivated simCards");
    }
}
