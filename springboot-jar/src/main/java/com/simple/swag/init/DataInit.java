package com.simple.swag.init;

import com.simple.swag.exception.ServiceException;
import com.simple.swag.service.PackService;
import com.swagger.model.SExistPack;
import com.swagger.model.SNewExistPack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataInit implements ApplicationRunner {

    private PackService packService;

    @Autowired
    public void setPackService(PackService packService) {
        this.packService = packService;
    }

    @Override
    public void run(ApplicationArguments applicationArguments) throws Exception {
        try {
            packService.findExistPackById(1L);
        } catch (ServiceException e){
            //Create standard pack
            SNewExistPack sNewExistPack = new SNewExistPack();
            sNewExistPack.setMinutes(50);
            sNewExistPack.setBytes(1073741824L);
            sNewExistPack.setDayLimit(30);
            packService.addExistPack(sNewExistPack);
        }
    }
}
