package com.simple.swag.repository;

import com.simple.swag.entity.ExistPackEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExistPackRepository extends JpaRepository<ExistPackEntity, Long> {

    Page<ExistPackEntity> findAllByOrderByIdDesc(Pageable pageable);

    boolean existsByMinutesAndBytesAndDayLimit(Integer minutes, Long bytes, Integer dayLimit);
}
