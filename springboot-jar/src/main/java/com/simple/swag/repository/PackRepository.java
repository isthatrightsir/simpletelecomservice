package com.simple.swag.repository;

import com.simple.swag.entity.PackEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PackRepository extends JpaRepository<PackEntity, Long> {

    @Query("SELECT p FROM PackEntity p JOIN p.simCardEntity s WHERE (p.status = 'AVAILABLE' AND p.dateEnd > :current_date) AND s.id = :id_h ORDER BY p.dateEnd ASC")
    Page<PackEntity> findAllAvailableBySimCardIdOrderByDateEndAsc(@Param("id_h")Long simCardId, @Param("current_date")Long currentDate, Pageable pageable);

    @Query("SELECT p FROM PackEntity p JOIN p.simCardEntity s WHERE (p.status = 'AVAILABLE' AND p.dateEnd > :current_date) AND s.id = :id_h")
    Page<PackEntity> findAllAvailableBySimCardId(@Param("id_h")Long simCardId, @Param("current_date")Long currentDate, Pageable pageable);

    @Query("SELECT p FROM PackEntity p JOIN p.simCardEntity s WHERE (p.status = 'AVAILABLE' AND p.dateEnd < :current_date) ORDER BY p.dateEnd DESC")
    List<PackEntity> findAllUnavailableOrderByIdDesc(@Param("current_date")Long currentDate);

    @Query("SELECT p FROM PackEntity p WHERE p.status = 'AVAILABLE' AND p.dateEnd > :current_date AND p.id = :id_h")
    PackEntity findAvailableByIdAndStatus(@Param("id_h")Long id, @Param("current_date")Long currentDate);

    //TODO: possible use without Query annotation
//    PackEntity findByStatusIsAvailableAndIdAndDateEndAfter(Long id, Long currentDate);

    void deleteByStatus(String status);
}
