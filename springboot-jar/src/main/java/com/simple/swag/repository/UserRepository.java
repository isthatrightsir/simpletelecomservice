package com.simple.swag.repository;

import com.simple.swag.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Page<UserEntity> findAllByOrderByPassportNumberAsc(Pageable pageable);

    UserEntity findByPassportNumber(Long passportNumber);

    boolean existsByPassportNumber(Long passportNumber);
}
