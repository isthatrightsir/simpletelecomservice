package com.simple.swag.repository;

import com.simple.swag.entity.SimCardEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SimCardRepository extends JpaRepository<SimCardEntity, Long> {

    @Query("SELECT s FROM SimCardEntity s WHERE s.status = 'ACTIVATED' ORDER BY s.id DESC")
    Page<SimCardEntity> findAllActivatedByOrderByIdDesc(Pageable pageable);

    @Query("SELECT s FROM SimCardEntity s WHERE s.id = :id_h AND s.status = 'ACTIVATED'")
    SimCardEntity findActivatedById(@Param("id_h")Long id);

    //TODO: possible use without Query annotation
//    SimCardEntity findByStatusIsAvailableAndId(Long id);
//    Page<SimCardEntity> findAllByStatusIsAvailableOrderByOrderByIdDesc(Pageable pageable);

    void deleteByStatus(String status);
}
