package com.simple.swag.enumeration;

public enum SimCardStatus {
    ACTIVATED,
    DEACTIVATED
}
