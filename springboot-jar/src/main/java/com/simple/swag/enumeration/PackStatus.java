package com.simple.swag.enumeration;

/**
 * The PackStatus enumeration.
 */
public enum PackStatus {
    AVAILABLE,
    UNAVAILABLE,
    DEACTIVATED
}
