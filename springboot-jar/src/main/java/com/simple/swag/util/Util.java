package com.simple.swag.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.simple.swag.constant.Constants.TIME_DATE_PATTERN;

public class Util {

    public static Long getCurrentDate(){
        LocalDateTime dt = LocalDateTime.now();
        return Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt));
    }
}
