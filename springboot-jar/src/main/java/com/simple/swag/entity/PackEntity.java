package com.simple.swag.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "pack")
public class PackEntity implements Serializable {

    private Long id;
    private Integer minutes;
    private Long bytes;
    private Long dateEnd;
    private String status;
    private int version;
    private SimCardEntity simCardEntity;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "minutes", nullable = false)
    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    @Column(name = "bytes", nullable = false)
    public Long getBytes() {
        return bytes;
    }

    public void setBytes(Long bytes) {
        this.bytes = bytes;
    }

    @Column(name = "date_end", length = 10, nullable = false)
    public Long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Long dateEnd) {
        this.dateEnd = dateEnd;
    }

    @Column(name = "status", length = 12, nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Version
    @Column(name = "version")
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @ManyToOne
    @JoinColumn(name = "sim_card_id")
    public SimCardEntity getSimCardEntity() {
        return simCardEntity;
    }

    public void setSimCardEntity(SimCardEntity simCardEntity) {
        this.simCardEntity = simCardEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PackEntity that = (PackEntity) o;

        if (!id.equals(that.id)) return false;
        if (!dateEnd.equals(that.dateEnd)) return false;
        return status.equals(that.status);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + dateEnd.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "PackEntity{" +
                "id=" + id +
                ", minutes=" + minutes +
                ", bytes=" + bytes +
                ", dateEnd='" + dateEnd + '\'' +
                ", status='" + status + '\'' +
                ", sim_id=" + (simCardEntity != null ? simCardEntity.getId().toString() : "Unknown") +
                '}';
    }
}
