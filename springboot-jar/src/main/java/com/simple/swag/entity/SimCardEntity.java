package com.simple.swag.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sim_card")
public class SimCardEntity implements Serializable {

    private Long id;
    private String status;
    private int version;
    private UserEntity userEntity;
    private Set<PackEntity> packEntities = new HashSet<>();

    @Id
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "status", length = 12, nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Version
    @Column(name = "version")
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    @OneToMany(mappedBy = "simCardEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    public Set<PackEntity> getPackEntities() {
        return packEntities;
    }

    public void setPackEntities(Set<PackEntity> packEntities) {
        this.packEntities = packEntities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimCardEntity simCardEntity = (SimCardEntity) o;

        if (!id.equals(simCardEntity.id)) return false;
        return status.equals(simCardEntity.status);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SimCardEntity{" +
                "id=" + id +
                ", status='" + status + '\'' +
                ", user_id=" + (userEntity != null ? userEntity.getId().toString() : "Unknown") +
                '}';
    }
}
