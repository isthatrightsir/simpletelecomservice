package com.simple.swag.entity;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "exist_pack")
public class ExistPackEntity implements Serializable {
    private Long id;
    private Integer minutes;
    private Long bytes;
    private Integer dayLimit;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "minutes", nullable = false)
    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

    @Column(name = "bytes", nullable = false)
    public Long getBytes() {
        return bytes;
    }

    public void setBytes(Long bytes) {
        this.bytes = bytes;
    }

    @Column(name = "day_limit", nullable = false)
    public Integer getDayLimit() {
        return dayLimit;
    }

    public void setDayLimit(Integer dayLimit) {
        this.dayLimit = dayLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExistPackEntity that = (ExistPackEntity) o;

        if (!id.equals(that.id)) return false;
        if (!minutes.equals(that.minutes)) return false;
        if (!bytes.equals(that.bytes)) return false;
        return dayLimit.equals(that.dayLimit);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + minutes.hashCode();
        result = 31 * result + bytes.hashCode();
        result = 31 * result + dayLimit.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ExistPackEntity{" +
                "id=" + id +
                ", minutes=" + minutes +
                ", bytes=" + bytes +
                ", dayLimit=" + dayLimit +
                '}';
    }
}
