package com.simple.swag.scheduler.tasklet;

import com.simple.swag.service.PackService;
import com.simple.swag.service.SimCardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

public class CleanTasklet implements Tasklet {

    private static final Logger log = LoggerFactory.getLogger(CleanTasklet.class);

    private SimCardService simCardService;

    private PackService packService;

    @Autowired
    public void setSimCardService(SimCardService simCardService) {
        this.simCardService = simCardService;
    }

    @Autowired
    public void setPackService(PackService packService) {
        this.packService = packService;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        log.info("Start execute CleanJob");
        log.info("Execute method removeAllDeactivatedSimCards");
        simCardService.removeAllDeactivatedSimCards();
        log.info("Execute method markExpiredPacksAsUnavailable");
        packService.markExpiredPacksAsUnavailable();
        log.info("Execute method removedAllUnavailablePacks");
        packService.removedAllUnavailablePacks();
        log.info("Finish execute CleanJob");
        return RepeatStatus.FINISHED;
    }
}
