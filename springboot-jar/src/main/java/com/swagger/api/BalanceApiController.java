package com.swagger.api;

import com.simple.swag.exception.ServiceException;
import com.simple.swag.service.PackService;
import com.swagger.model.ErrorModel;
import com.swagger.model.TrafficModel;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

@Controller
public class BalanceApiController implements BalanceApi {

    private static final Logger log = LoggerFactory.getLogger(BalanceApiController.class);

    private final HttpServletRequest request;

    private PackService packService;

    @Autowired
    public void setPackService(PackService packService) {
        this.packService = packService;
    }

    @org.springframework.beans.factory.annotation.Autowired
    public BalanceApiController(HttpServletRequest request) {
        this.request = request;
    }

    public ResponseEntity<TrafficModel> getCurrentBalanceBySimCardId(@ApiParam(value = "ID of items",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                TrafficModel trafficModel = packService.getCurrentBalanceBySimCardId(id);
                return new ResponseEntity<TrafficModel>(trafficModel, HttpStatus.OK);
            } catch (ServiceException e){
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.NOT_FOUND.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.NOT_FOUND);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<TrafficModel>(HttpStatus.NOT_IMPLEMENTED);
    }

}
