package com.swagger.api;

import com.simple.swag.exception.ServiceException;
import com.simple.swag.service.PackService;
import com.swagger.model.ErrorModel;
import com.swagger.model.SExistPack;
import com.swagger.model.SNewExistPack;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
public class ExistPacksApiController implements ExistPacksApi {

    private static final Logger log = LoggerFactory.getLogger(ExistPacksApiController.class);

    private final HttpServletRequest request;

    private PackService packService;

    @Autowired
    public void setPackService(PackService packService) {
        this.packService = packService;
    }

    @org.springframework.beans.factory.annotation.Autowired
    public ExistPacksApiController(HttpServletRequest request) {
        this.request = request;
    }

    public ResponseEntity<Long> addExistPack(@ApiParam(value = "Add new existPack" ,required=true )  @Valid @RequestBody SNewExistPack newExistPack) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Long id = packService.addExistPack(newExistPack);
                return new ResponseEntity<Long>(id, HttpStatus.OK);
            } catch (ServiceException e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.CONFLICT.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.CONFLICT);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Long>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<SExistPack>> findAllExistPacks(@ApiParam(value = "Number of page") @Valid @RequestParam(value = "pageNumber", required = false) Integer pageNumber,@ApiParam(value = "Page size") @Valid @RequestParam(value = "pageSize", required = false) Integer pageSize) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Page<SExistPack> result = packService.findAllExistPacks(pageNumber == null ? 0 : pageNumber, pageSize == null ? 0 : pageSize);
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.set("Total-Elements", String.valueOf(result.getTotalElements()));
                responseHeaders.set("Total-Pages", String.valueOf(result.getTotalPages()));
                responseHeaders.set("Pagination-Page", String.valueOf(pageNumber));
                responseHeaders.set("Pagination-Size", String.valueOf(pageSize));
                return new ResponseEntity<List<SExistPack>>(result.getContent(), responseHeaders, HttpStatus.OK);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<SExistPack>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<SExistPack> findExistPackById(@ApiParam(value = "ID of items",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                SExistPack sExistPack = packService.findExistPackById(id);
                return new ResponseEntity<SExistPack>(sExistPack, HttpStatus.OK);
            } catch (ServiceException e){
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.NOT_FOUND.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.NOT_FOUND);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<SExistPack>(HttpStatus.NOT_IMPLEMENTED);
    }

}
