package com.swagger.api;

import com.simple.swag.exception.ServiceException;
import com.simple.swag.service.PackService;
import com.swagger.model.ErrorModel;
import com.swagger.model.SPack;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
public class PacksApiController implements PacksApi {

    private static final Logger log = LoggerFactory.getLogger(PacksApiController.class);

    private final HttpServletRequest request;

    private PackService packService;

    @Autowired
    public void setPackService(PackService packService) {
        this.packService = packService;
    }

    @org.springframework.beans.factory.annotation.Autowired
    public PacksApiController(HttpServletRequest request) {
        this.request = request;
    }

    public ResponseEntity<Long> addPackToActivatedSimCard(@NotNull @ApiParam(value = "ID of simCard", required = true) @Valid @RequestParam(value = "simid", required = true) Long simid,@NotNull @ApiParam(value = "ID of existPack", required = true) @Valid @RequestParam(value = "expackid", required = true) Long expackid) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Long id = packService.addPackToActivatedSimCard(simid, expackid);
                return new ResponseEntity<Long>(id, HttpStatus.OK);
            } catch (ServiceException e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.CONFLICT.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.CONFLICT);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Long>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<SPack>> findAllAvailableByActivatedSimCardId(@NotNull @ApiParam(value = "ID of activated simCard", required = true) @Valid @RequestParam(value = "id", required = true) Long id,@ApiParam(value = "Number of page") @Valid @RequestParam(value = "pageNumber", required = false) Integer pageNumber,@ApiParam(value = "Page size") @Valid @RequestParam(value = "pageSize", required = false) Integer pageSize) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Page<SPack> result = packService.findAllAvailableByActivatedSimCardId(id, pageNumber == null ? 0 : pageNumber, pageSize == null ? 0 : pageSize);
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.set("Total-Elements", String.valueOf(result.getTotalElements()));
                responseHeaders.set("Total-Pages", String.valueOf(result.getTotalPages()));
                responseHeaders.set("Pagination-Page", String.valueOf(pageNumber));
                responseHeaders.set("Pagination-Size", String.valueOf(pageSize));
                responseHeaders.set("SimCard-Id", String.valueOf(id));
                return new ResponseEntity<List<SPack>>(result.getContent(), responseHeaders, HttpStatus.OK);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<SPack>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<SPack> findAvailablePackById(@ApiParam(value = "ID of items",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                SPack sPack = packService.findAvailablePackById(id);
                return new ResponseEntity<SPack>(sPack, HttpStatus.OK);
            } catch (ServiceException e){
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.NOT_FOUND.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.NOT_FOUND);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<SPack>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updatePackBySimCardId(@NotNull @ApiParam(value = "ID of simCard", required = true) @Valid @RequestParam(value = "simid", required = true) Long simid,@NotNull @ApiParam(value = "Spent minutes", required = true) @Valid @RequestParam(value = "minutes", required = true) Integer minutes,@NotNull @ApiParam(value = "Spent bytes", required = true) @Valid @RequestParam(value = "bytes", required = true) Long bytes) {
        String accept = request.getHeader("Accept");
        try {
            packService.updatePackBySimCardId(simid, minutes, bytes);
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            ErrorModel errorModel = new ErrorModel();
            errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            errorModel.setMessage(e.getMessage());
            return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
