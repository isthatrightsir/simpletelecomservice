package com.swagger.api;

import com.simple.swag.exception.ServiceException;
import com.simple.swag.service.SimCardService;
import com.swagger.model.ErrorModel;
import com.swagger.model.SSimCard;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Controller
public class SimCardsApiController implements SimCardsApi {

    private static final Logger log = LoggerFactory.getLogger(SimCardsApiController.class);

    private final HttpServletRequest request;

    private SimCardService simCardService;

    @Autowired
    public void setSimCardService(SimCardService simCardService) {
        this.simCardService = simCardService;
    }

    @org.springframework.beans.factory.annotation.Autowired
    public SimCardsApiController(HttpServletRequest request) {
        this.request = request;
    }

    public ResponseEntity<Long> addSimCardByUserPassportNumber(@NotNull @ApiParam(value = "Passport number of user", required = true) @Valid @RequestParam(value = "passportNumber", required = true) Long passportNumber) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Long sim_id = simCardService.addSimCardByUserPassportNumber(passportNumber);
                return new ResponseEntity<Long>(sim_id, HttpStatus.OK);
            } catch (ServiceException e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.CONFLICT.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.CONFLICT);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Long>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> deactivatedSimCardById(@ApiParam(value = "ID of items",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        try {
            simCardService.markAsDeactivatedSimCardById(id);
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            ErrorModel errorModel = new ErrorModel();
            errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            errorModel.setMessage(e.getMessage());
            return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<SSimCard> findActivatedSimCardById(@ApiParam(value = "ID of items",required=true) @PathVariable("id") Long id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                SSimCard sSimCard = simCardService.findActivatedSimCardById(id);
                return new ResponseEntity<SSimCard>(sSimCard, HttpStatus.OK);
            } catch (ServiceException e){
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.NOT_FOUND.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.NOT_FOUND);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<SSimCard>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<SSimCard>> findAllActivatedSimCard(@ApiParam(value = "Number of page") @Valid @RequestParam(value = "pageNumber", required = false) Integer pageNumber,@ApiParam(value = "Page size") @Valid @RequestParam(value = "pageSize", required = false) Integer pageSize) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Page<SSimCard> result = simCardService.findAllActivatedSimCard(pageNumber == null ? 0 : pageNumber, pageSize == null ? 0 : pageSize);
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.set("Total-Elements", String.valueOf(result.getTotalElements()));
                responseHeaders.set("Total-Pages", String.valueOf(result.getTotalPages()));
                responseHeaders.set("Pagination-Page", String.valueOf(pageNumber));
                responseHeaders.set("Pagination-Size", String.valueOf(pageSize));
                return new ResponseEntity<List<SSimCard>>(result.getContent(), responseHeaders, HttpStatus.OK);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<SSimCard>>(HttpStatus.NOT_IMPLEMENTED);
    }

}
