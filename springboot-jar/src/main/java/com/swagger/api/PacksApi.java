/**
 * NOTE: This class is auto generated by the swagger code generator program (2.4.7).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package com.swagger.api;

import com.swagger.model.ErrorModel;
import com.swagger.model.SPack;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

@Api(value = "packs", description = "the packs API", tags = "Pack")
public interface PacksApi {

    @ApiOperation(value = "", nickname = "addPackToActivatedSimCard", notes = "Add new pack by activated simCard id", response = Long.class, tags={ "Pack", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Returns id of new pack", response = Long.class),
        @ApiResponse(code = 409, message = "Conflict with add new pack", response = ErrorModel.class),
        @ApiResponse(code = 500, message = "Unexpected error", response = ErrorModel.class),
        @ApiResponse(code = 501, message = "Not implemented", response = ErrorModel.class) })
    @RequestMapping(value = "/packs",
        produces = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<Long> addPackToActivatedSimCard(@NotNull @ApiParam(value = "ID of simCard", required = true) @Valid @RequestParam(value = "simid", required = true) Long simid,@NotNull @ApiParam(value = "ID of existPack", required = true) @Valid @RequestParam(value = "expackid", required = true) Long expackid);


    @ApiOperation(value = "", nickname = "findAllAvailableByActivatedSimCardId", notes = "Returns all available packs from the database by simCard ID with pagination order by DateEnd Desc", response = SPack.class, responseContainer = "List", tags={ "Pack", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "List of available packs response with metadata in headers", response = SPack.class, responseContainer = "List"),
        @ApiResponse(code = 500, message = "Unexpected error", response = ErrorModel.class),
        @ApiResponse(code = 501, message = "Not implemented", response = ErrorModel.class) })
    @RequestMapping(value = "/packs",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<List<SPack>> findAllAvailableByActivatedSimCardId(@NotNull @ApiParam(value = "ID of activated simCard", required = true) @Valid @RequestParam(value = "id", required = true) Long id,@ApiParam(value = "Number of page") @Valid @RequestParam(value = "pageNumber", required = false) Integer pageNumber,@ApiParam(value = "Page size") @Valid @RequestParam(value = "pageSize", required = false) Integer pageSize);


    @ApiOperation(value = "", nickname = "findAvailablePackById", notes = "Returns a available pack by id", response = SPack.class, tags={ "Pack", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Pack response", response = SPack.class),
        @ApiResponse(code = 404, message = "Available pack not found", response = ErrorModel.class),
        @ApiResponse(code = 500, message = "Unexpected error", response = ErrorModel.class),
        @ApiResponse(code = 501, message = "Not implemented", response = ErrorModel.class) })
    @RequestMapping(value = "/packs/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    ResponseEntity<SPack> findAvailablePackById(@ApiParam(value = "ID of items",required=true) @PathVariable("id") Long id);


    @ApiOperation(value = "", nickname = "updatePackBySimCardId", notes = "Update balance(minutes/bytes) pack only. Without add record to database", tags={ "Pack", })
    @ApiResponses(value = { 
        @ApiResponse(code = 204, message = "Pack updated"),
        @ApiResponse(code = 500, message = "Unexpected error", response = ErrorModel.class),
        @ApiResponse(code = 501, message = "Not implemented", response = ErrorModel.class) })
    @RequestMapping(value = "/packs",
        produces = { "application/json" },
        method = RequestMethod.PUT)
    ResponseEntity<Void> updatePackBySimCardId(@NotNull @ApiParam(value = "ID of simCard", required = true) @Valid @RequestParam(value = "simid", required = true) Long simid,@NotNull @ApiParam(value = "Spent minutes", required = true) @Valid @RequestParam(value = "minutes", required = true) Integer minutes,@NotNull @ApiParam(value = "Spent bytes", required = true) @Valid @RequestParam(value = "bytes", required = true) Long bytes);

}
