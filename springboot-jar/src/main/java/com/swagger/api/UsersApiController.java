package com.swagger.api;

import com.simple.swag.exception.ServiceException;
import com.simple.swag.service.UserService;
import com.swagger.model.ErrorModel;
import com.swagger.model.SNewUser;
import com.swagger.model.SUser;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class UsersApiController implements UsersApi {

    private static final Logger log = LoggerFactory.getLogger(UsersApiController.class);

    private final HttpServletRequest request;

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @org.springframework.beans.factory.annotation.Autowired
    public UsersApiController(HttpServletRequest request) {
        this.request = request;
    }

    public ResponseEntity<Long> addUser(@ApiParam(value = "Add new User, who will have SimCard and 1 default Pack" ,required=true )  @Valid @RequestBody SNewUser newUser) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Long id = userService.addNewUser(newUser);
                return new ResponseEntity<Long>(id, HttpStatus.OK);
            } catch (ServiceException e){
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.CONFLICT.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.CONFLICT);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Long>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<List<SUser>> findAllUsers(@ApiParam(value = "Number of page") @Valid @RequestParam(value = "pageNumber", required = false) Integer pageNumber,@ApiParam(value = "Page size") @Valid @RequestParam(value = "pageSize", required = false) Integer pageSize) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Page<SUser> result = userService.findAllUsers(pageNumber == null ? 0 : pageNumber, pageSize == null ? 0 : pageSize);
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.set("Total-Elements", String.valueOf(result.getTotalElements()));
                responseHeaders.set("Total-Pages", String.valueOf(result.getTotalPages()));
                responseHeaders.set("Pagination-Page", String.valueOf(pageNumber));
                responseHeaders.set("Pagination-Size", String.valueOf(pageSize));
                return new ResponseEntity<List<SUser>>(result.getContent(), responseHeaders, HttpStatus.OK);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<SUser>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<SUser> findUserByPassportNumber(@ApiParam(value = "Passport number of user",required=true) @PathVariable("passportNumber") Long passportNumber) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                SUser sUser = userService.findUserByPassportNumber(passportNumber);
                return new ResponseEntity<SUser>(sUser, HttpStatus.OK);
            } catch (ServiceException e){
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.NOT_FOUND.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.NOT_FOUND);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                ErrorModel errorModel = new ErrorModel();
                errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
                errorModel.setMessage(e.getMessage());
                return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<SUser>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> removeUserByPassportNumber(@ApiParam(value = "Passport number of user",required=true) @PathVariable("passportNumber") Long passportNumber) {
        String accept = request.getHeader("Accept");
        try {
            userService.removeUserByPassportNumber(passportNumber);
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            ErrorModel errorModel = new ErrorModel();
            errorModel.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
            errorModel.setMessage(e.getMessage());
            return new ResponseEntity(errorModel, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
