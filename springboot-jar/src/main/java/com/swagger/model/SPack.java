package com.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SPack
 */
@Validated

public class SPack   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("minutes")
  private Integer minutes = null;

  @JsonProperty("bytes")
  private Long bytes = null;

  @JsonProperty("dateEnd")
  private Long dateEnd = null;

  @JsonProperty("status")
  private String status = null;

  public SPack id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public SPack minutes(Integer minutes) {
    this.minutes = minutes;
    return this;
  }

  /**
   * Get minutes
   * @return minutes
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getMinutes() {
    return minutes;
  }

  public void setMinutes(Integer minutes) {
    this.minutes = minutes;
  }

  public SPack bytes(Long bytes) {
    this.bytes = bytes;
    return this;
  }

  /**
   * Get bytes
   * @return bytes
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Long getBytes() {
    return bytes;
  }

  public void setBytes(Long bytes) {
    this.bytes = bytes;
  }

  public SPack dateEnd(Long dateEnd) {
    this.dateEnd = dateEnd;
    return this;
  }

  /**
   * Get dateEnd
   * @return dateEnd
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Long getDateEnd() {
    return dateEnd;
  }

  public void setDateEnd(Long dateEnd) {
    this.dateEnd = dateEnd;
  }

  public SPack status(String status) {
    this.status = status;
    return this;
  }

  /**
   * Get status
   * @return status
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SPack spack = (SPack) o;
    return Objects.equals(this.id, spack.id) &&
        Objects.equals(this.minutes, spack.minutes) &&
        Objects.equals(this.bytes, spack.bytes) &&
        Objects.equals(this.dateEnd, spack.dateEnd) &&
        Objects.equals(this.status, spack.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, minutes, bytes, dateEnd, status);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SPack {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    minutes: ").append(toIndentedString(minutes)).append("\n");
    sb.append("    bytes: ").append(toIndentedString(bytes)).append("\n");
    sb.append("    dateEnd: ").append(toIndentedString(dateEnd)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

