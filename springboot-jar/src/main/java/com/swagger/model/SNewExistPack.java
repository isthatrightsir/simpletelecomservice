package com.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SNewExistPack
 */
@Validated

public class SNewExistPack   {
  @JsonProperty("minutes")
  private Integer minutes = null;

  @JsonProperty("bytes")
  private Long bytes = null;

  @JsonProperty("dayLimit")
  private Integer dayLimit = null;

  public SNewExistPack minutes(Integer minutes) {
    this.minutes = minutes;
    return this;
  }

  /**
   * Get minutes
   * @return minutes
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getMinutes() {
    return minutes;
  }

  public void setMinutes(Integer minutes) {
    this.minutes = minutes;
  }

  public SNewExistPack bytes(Long bytes) {
    this.bytes = bytes;
    return this;
  }

  /**
   * Get bytes
   * @return bytes
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Long getBytes() {
    return bytes;
  }

  public void setBytes(Long bytes) {
    this.bytes = bytes;
  }

  public SNewExistPack dayLimit(Integer dayLimit) {
    this.dayLimit = dayLimit;
    return this;
  }

  /**
   * Get dayLimit
   * @return dayLimit
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Integer getDayLimit() {
    return dayLimit;
  }

  public void setDayLimit(Integer dayLimit) {
    this.dayLimit = dayLimit;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SNewExistPack snewExistPack = (SNewExistPack) o;
    return Objects.equals(this.minutes, snewExistPack.minutes) &&
        Objects.equals(this.bytes, snewExistPack.bytes) &&
        Objects.equals(this.dayLimit, snewExistPack.dayLimit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(minutes, bytes, dayLimit);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SNewExistPack {\n");
    
    sb.append("    minutes: ").append(toIndentedString(minutes)).append("\n");
    sb.append("    bytes: ").append(toIndentedString(bytes)).append("\n");
    sb.append("    dayLimit: ").append(toIndentedString(dayLimit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

