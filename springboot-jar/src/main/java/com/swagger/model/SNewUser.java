package com.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * SNewUser
 */
@Validated

public class SNewUser   {
  @JsonProperty("passportNumber")
  private Long passportNumber = null;

  @JsonProperty("email")
  private String email = null;

  public SNewUser passportNumber(Long passportNumber) {
    this.passportNumber = passportNumber;
    return this;
  }

  /**
   * Get passportNumber
   * @return passportNumber
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Long getPassportNumber() {
    return passportNumber;
  }

  public void setPassportNumber(Long passportNumber) {
    this.passportNumber = passportNumber;
  }

  public SNewUser email(String email) {
    this.email = email;
    return this;
  }

  /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SNewUser snewUser = (SNewUser) o;
    return Objects.equals(this.passportNumber, snewUser.passportNumber) &&
        Objects.equals(this.email, snewUser.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(passportNumber, email);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SNewUser {\n");
    
    sb.append("    passportNumber: ").append(toIndentedString(passportNumber)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

