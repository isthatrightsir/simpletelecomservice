package com.simple.swag.service;

import com.simple.swag.converter.UserConverter;
import com.simple.swag.entity.UserEntity;
import com.simple.swag.exception.ServiceException;
import com.simple.swag.repository.UserRepository;
import com.swagger.model.SNewUser;
import com.swagger.model.SUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private SimCardService simCardService;

    @InjectMocks
    private UserService userService;

    @Test
    public void findAllUsers(){
        // given
        UserEntity userEntity = new UserEntity();
        userEntity.setId(0L);
        userEntity.setPassportNumber(12345L);
        userEntity.setEmail("test@gmail.com");
        userEntity.setVersion(0);
        UserEntity userEntity1 = new UserEntity();
        userEntity1.setId(1L);
        userEntity1.setPassportNumber(123456L);
        userEntity1.setEmail("test1@gmail.com");
        userEntity1.setVersion(0);

        List<UserEntity> userEntities = new ArrayList<>(Arrays.asList(userEntity, userEntity1));
        Page<UserEntity> expectedUserEntities = new PageImpl<>(userEntities);
        Page<SUser> expectedResult = expectedUserEntities.map(new UserConverter());
        final int page = 0;
        final int size = 2;
        final Pageable pageable = PageRequest.of(page, size);

        // mock
        doReturn(expectedUserEntities).when(userRepository).findAllByOrderByPassportNumberAsc(pageable);

        // when
        Page<SUser> result = userService.findAllUsers(page, size);

        // then
        assertThat(result.getTotalElements()).isEqualTo(expectedResult.getTotalElements());
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void findUserByPassportNumberExist() throws ServiceException {
        // given
        UserEntity userEntity = new UserEntity();
        userEntity.setPassportNumber(12345L);
        userEntity.setEmail("test@gmail.com");
        userEntity.setVersion(0);
        SUser expectedResult = new UserConverter().apply(userEntity);

        // mock
        doReturn(userEntity).when(userRepository).findByPassportNumber(12345L);

        // when
        SUser result = userService.findUserByPassportNumber(12345L);

        // then
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test(expected = ServiceException.class)
    public void findUserByPassportNumberNotExist() throws ServiceException {
        // mock
        doReturn(null).when(userRepository).findByPassportNumber(12345L);

        // then throw ServiceException
        userService.findUserByPassportNumber(12345L);
    }

    @Test
    public void addNewUserNotExist() throws ServiceException {
        // given
        SNewUser sNewUser = new SNewUser();
        sNewUser.setPassportNumber(12345L);
        sNewUser.setEmail("test@gmail.com");
        UserEntity userEntity = UserConverter.unconvert(sNewUser);
        UserEntity userEntity1 = UserConverter.unconvert(sNewUser);
        userEntity1.setId(0L);

        // mock
        doReturn(userEntity1).when(userRepository).save(userEntity);
        doReturn(false).when(userRepository).existsByPassportNumber(12345L);

        // when
        Long result = userService.addNewUser(sNewUser);

        // then
        assertThat(result).isEqualTo(0L);
    }

    @Test(expected = ServiceException.class)
    public void addNewUserExist() throws ServiceException {
        // given
        SNewUser sNewUser = new SNewUser();
        sNewUser.setPassportNumber(12345L);
        sNewUser.setEmail("test@gmail.com");
        UserEntity userEntity = UserConverter.unconvert(sNewUser);

        // mock
        doReturn(true).when(userRepository).existsByPassportNumber(12345L);

        // then throw ServiceException
        userService.addNewUser(sNewUser);
    }

    @Test
    public void removeUserByPassportNumber() throws ServiceException {
        // given
        UserEntity userEntity = new UserEntity();
        userEntity.setPassportNumber(12345L);
        userEntity.setEmail("test@gmail.com");
        userEntity.setVersion(0);
        SUser expectedResult = new UserConverter().apply(userEntity);

        // mock
        doReturn(userEntity).when(userRepository).findByPassportNumber(12345L);

        // when
        userService.removeUserByPassportNumber(12345L);
    }
}
