package com.simple.swag.service;

import com.simple.swag.converter.PackConvert;
import com.simple.swag.entity.PackEntity;
import com.simple.swag.entity.SimCardEntity;
import com.simple.swag.enumeration.PackStatus;
import com.simple.swag.enumeration.SimCardStatus;
import com.simple.swag.exception.ServiceException;
import com.simple.swag.repository.PackRepository;
import com.simple.swag.repository.SimCardRepository;
import com.simple.swag.util.Util;
import com.swagger.model.SPack;
import com.swagger.model.TrafficModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doReturn;
import static com.simple.swag.constant.Constants.TIME_DATE_PATTERN;

@RunWith(MockitoJUnitRunner.class)
public class PackServiceTest {

    @Mock
    private PackRepository packRepository;

    @Mock
    private SimCardRepository simCardRepository;

    @InjectMocks
    private PackService packService;

    @Test
    public void findAllAvailableByActivatedSimCardId() throws ServiceException {
        // given
        SimCardEntity simCardEntity = new SimCardEntity();
        simCardEntity.setId(1L);
        simCardEntity.setStatus(SimCardStatus.ACTIVATED.name());
        PackEntity packEntity = new PackEntity();
        packEntity.setId(0L);
        packEntity.setMinutes(30);
        packEntity.setBytes(1024L);
        LocalDateTime dt = LocalDateTime.now().plusDays(1); //plus days
        Long dateEnd = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt));
        packEntity.setDateEnd(dateEnd);
        packEntity.setStatus(PackStatus.AVAILABLE.name());
        PackEntity packEntity1 = new PackEntity();
        packEntity1.setId(1L);
        packEntity1.setMinutes(30);
        packEntity1.setBytes(1024L);
        LocalDateTime dt1 = LocalDateTime.now().plusDays(2); //plus days
        Long dateEnd1 = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt1));
        packEntity1.setDateEnd(dateEnd1);
        packEntity1.setStatus(PackStatus.AVAILABLE.name());

        List<PackEntity> packEntities = new ArrayList<>(Arrays.asList(packEntity, packEntity1));
        Page<PackEntity> expectedPackEntities = new PageImpl<>(packEntities);
        Page<SPack> expectedResult = expectedPackEntities.map(new PackConvert());
        final int page = 0;
        final int size = 2;
        final Pageable pageable = PageRequest.of(page, size);
        final Long currentDate = Util.getCurrentDate();

        // mock
        doReturn(simCardEntity).when(simCardRepository).findActivatedById(1L);
        doReturn(expectedPackEntities).when(packRepository).findAllAvailableBySimCardIdOrderByDateEndAsc(1L, currentDate, pageable);

        // when
        Page<SPack> result = packService.findAllAvailableByActivatedSimCardId(1L, page, size);

        // then
        assertThat(result.getTotalElements()).isEqualTo(expectedResult.getTotalElements());
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void findAvailablePackById() throws ServiceException {
        // given
        PackEntity packEntity = new PackEntity();
        packEntity.setId(0L);
        packEntity.setMinutes(30);
        packEntity.setBytes(1024L);
        LocalDateTime dt = LocalDateTime.now().plusDays(1); //plus days
        Long dateEnd = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt));
        packEntity.setDateEnd(dateEnd);
        packEntity.setStatus(PackStatus.AVAILABLE.name());
        SPack expectedResult = new PackConvert().apply(packEntity);
        final Long currentDate = Util.getCurrentDate();

        // mock
        doReturn(packEntity).when(packRepository).findAvailableByIdAndStatus(0L, currentDate);

        // when
        SPack result = packService.findAvailablePackById(0L);

        // then
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void balance() throws ServiceException {
        // given
        SimCardEntity simCardEntity = new SimCardEntity();
        simCardEntity.setId(1L);
        PackEntity packEntity = new PackEntity();
        packEntity.setId(0L);
        packEntity.setMinutes(10);
        packEntity.setBytes(0L);
        LocalDateTime dt = LocalDateTime.now().plusDays(1);
        Long dateEnd = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt));
        packEntity.setDateEnd(dateEnd);
        packEntity.setStatus(PackStatus.AVAILABLE.name());
        PackEntity packEntity1 = new PackEntity();
        packEntity1.setId(1L);
        packEntity1.setMinutes(0);
        packEntity1.setBytes(10L);
        LocalDateTime dt1 = LocalDateTime.now().plusDays(2);
        Long dateEnd1 = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt1));
        packEntity1.setDateEnd(dateEnd1);
        packEntity1.setStatus(PackStatus.AVAILABLE.name());
        PackEntity packEntity2 = new PackEntity();
        packEntity2.setId(2L);
        packEntity2.setMinutes(0);
        packEntity2.setBytes(10L);
        LocalDateTime dt2 = LocalDateTime.now().plusDays(3);
        Long dateEnd2 = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt2));
        packEntity2.setDateEnd(dateEnd2);
        packEntity2.setStatus(PackStatus.UNAVAILABLE.name());
        PackEntity packEntity3 = new PackEntity();
        packEntity3.setId(3L);
        packEntity3.setMinutes(10);
        packEntity3.setBytes(0L);
        LocalDateTime dt3 = LocalDateTime.now().plusDays(4);
        Long dateEnd3 = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt3));
        packEntity3.setDateEnd(dateEnd3);
        packEntity3.setStatus(PackStatus.AVAILABLE.name());
        List<PackEntity> packEntities = new ArrayList<>(Arrays.asList(packEntity, packEntity1, packEntity2, packEntity3));
        Page<PackEntity> expectedPackEntities = new PageImpl<>(packEntities);

        // mock
        doReturn(simCardEntity).when(simCardRepository).findActivatedById(1L);
        doReturn(expectedPackEntities).when(packRepository).findAllAvailableBySimCardIdOrderByDateEndAsc(anyLong(), anyLong(), (Pageable) isNull());
        doReturn(expectedPackEntities).when(packRepository).findAllAvailableBySimCardId(anyLong(), anyLong(), (Pageable) isNull());

        // when
        TrafficModel trafficModel = packService.getCurrentBalanceBySimCardId(1L);
        packService.updatePackBySimCardId(1L, 20, 20L);

        // then
        assertThat(trafficModel.getMinutes()).isEqualTo(20);
        assertThat(trafficModel.getBytes()).isEqualTo(20);
    }
}
