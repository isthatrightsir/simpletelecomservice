package com.simple.swag.service;

import com.simple.swag.converter.SimCardConvert;
import com.simple.swag.entity.SimCardEntity;
import com.simple.swag.enumeration.SimCardStatus;
import com.simple.swag.exception.ServiceException;
import com.simple.swag.repository.SimCardRepository;
import com.simple.swag.repository.UserRepository;
import com.swagger.model.SSimCard;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class SimCardServiceTest {

    @Mock
    private SimCardRepository simCardRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private SimCardService simCardService;

    @Test
    public void findAllActivatedSimCard(){
        // given
        SimCardEntity simCardEntity = new SimCardEntity();
        simCardEntity.setId(0L);
        simCardEntity.setStatus(SimCardStatus.ACTIVATED.name());
        simCardEntity.setVersion(0);
        SimCardEntity simCardEntity1 = new SimCardEntity();
        simCardEntity1.setId(1L);
        simCardEntity1.setStatus(SimCardStatus.ACTIVATED.name());
        simCardEntity1.setVersion(0);

        List<SimCardEntity> simCardEntities = new ArrayList<>(Arrays.asList(simCardEntity1, simCardEntity));
        Page<SimCardEntity> expectedSimCardEntities = new PageImpl<>(simCardEntities);
        Page<SSimCard> expectedResult = expectedSimCardEntities.map(new SimCardConvert());
        final int page = 0;
        final int size = 2;
        final Pageable pageable = PageRequest.of(page, size);

        // mock
        doReturn(expectedSimCardEntities).when(simCardRepository).findAllActivatedByOrderByIdDesc(pageable);

        // when
        Page<SSimCard> result = simCardService.findAllActivatedSimCard(page, size);

        // then
        assertThat(result.getTotalElements()).isEqualTo(expectedResult.getTotalElements());
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    public void findActivatedSimCardByIdExist() throws ServiceException {
        // given
        SimCardEntity simCardEntity = new SimCardEntity();
        simCardEntity.setId(0L);
        simCardEntity.setStatus(SimCardStatus.ACTIVATED.name());
        simCardEntity.setVersion(0);
        SSimCard expectedResult = new SimCardConvert().apply(simCardEntity);

        // mock
        doReturn(simCardEntity).when(simCardRepository).findActivatedById(0L);

        // when
        SSimCard result = simCardService.findActivatedSimCardById(0L);

        // then
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test(expected = ServiceException.class)
    public void findActivatedSimCardByIdNotExist() throws ServiceException {
        // mock
        doReturn(null).when(simCardRepository).findActivatedById(0L);

        // then throw ServiceException
        simCardService.findActivatedSimCardById(0L);
    }

    @Test(expected = ServiceException.class)
    public void addSimCardByUserPassportNumberNotExistUser() throws ServiceException {
        // mock
        doReturn(null).when(userRepository).findByPassportNumber(12345L);

        // then throw ServiceException
        simCardService.addSimCardByUserPassportNumber(12345L);
    }
}
