package com.simple.swag.controller;

import com.simple.swag.enumeration.PackStatus;
import com.simple.swag.service.PackService;
import com.swagger.Swagger2SpringBoot;
import com.swagger.api.UsersApiController;
import com.swagger.model.SExistPack;
import com.swagger.model.SPack;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.simple.swag.constant.Constants.TIME_DATE_PATTERN;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(UsersApiController.class)
@ContextConfiguration(classes = Swagger2SpringBoot.class)
public class PackControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PackService packService;

    @Test
    public void findAllAvailableByActivatedSimCardId() throws Exception {
        // given
        SPack sPack = new SPack();
        sPack.setId(1L);
        sPack.setMinutes(10);
        sPack.setBytes(1024L);
        sPack.setStatus(PackStatus.AVAILABLE.name());
        LocalDateTime dt = LocalDateTime.now().plusDays(1);
        Long dateEnd = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt));
        sPack.setDateEnd(dateEnd);
        SPack sPack1 = new SPack();
        sPack1.setId(2L);
        sPack1.setMinutes(11);
        sPack1.setBytes(2048L);
        sPack1.setStatus(PackStatus.AVAILABLE.name());
        sPack1.setDateEnd(dateEnd);
        List<SPack> packEntities = new ArrayList<>(Arrays.asList(sPack, sPack1));
        Page<SPack> expectedResult = new PageImpl<>(packEntities);

        // mock
        doReturn(expectedResult).when(packService).findAllAvailableByActivatedSimCardId(12L,0,2);

        // when + then
        this.mockMvc.perform(get("/packs").accept("application/json")
                .param("id", "12")
                .param("pageNumber","0")
                .param("pageSize","2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(sPack.getId().intValue())));
    }

    @Test
    public void findAvailablePackById() throws Exception {
        // given
        SPack sPack = new SPack();
        sPack.setId(1L);
        sPack.setMinutes(10);
        sPack.setBytes(1024L);
        sPack.setStatus(PackStatus.AVAILABLE.name());
        LocalDateTime dt = LocalDateTime.now().plusDays(1);
        Long dateEnd = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt));
        sPack.setDateEnd(dateEnd);

        // mock
        doReturn(sPack).when(packService).findAvailablePackById(0L);

        // when + then
        this.mockMvc.perform(get("/packs/{id}",0L).accept("application/json")).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(sPack.getId().intValue())));
    }

    @Test
    public void findAllExistPacks() throws Exception {
        // given
        SExistPack sExistPack = new SExistPack();
        sExistPack.setId(0L);
        sExistPack.setMinutes(10);
        sExistPack.setBytes(1024L);
        sExistPack.setDayLimit(1);
        SExistPack sExistPack1 = new SExistPack();
        sExistPack1.setId(1L);
        sExistPack1.setMinutes(20);
        sExistPack1.setBytes(2048L);
        sExistPack1.setDayLimit(2);
        List<SExistPack> existPackEntities = new ArrayList<>(Arrays.asList(sExistPack, sExistPack1));
        Page<SExistPack> expectedResult = new PageImpl<>(existPackEntities);

        // mock
        doReturn(expectedResult).when(packService).findAllExistPacks(0,2);

        // when + then
        this.mockMvc.perform(get("/exist-packs").accept("application/json")
                .param("pageNumber","0")
                .param("pageSize","2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(sExistPack.getId().intValue())));
    }

    @Test
    public void findExistPackById() throws Exception {
        // given
        SExistPack sExistPack = new SExistPack();
        sExistPack.setId(0L);
        sExistPack.setMinutes(10);
        sExistPack.setBytes(1024L);
        sExistPack.setDayLimit(1);

        // mock
        doReturn(sExistPack).when(packService).findExistPackById(0L);

        // when + then
        this.mockMvc.perform(get("/exist-packs/{id}",0L).accept("application/json")).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(sExistPack.getId().intValue())));
    }
}
