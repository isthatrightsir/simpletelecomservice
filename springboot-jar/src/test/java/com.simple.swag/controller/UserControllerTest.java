package com.simple.swag.controller;

import com.simple.swag.service.UserService;
import com.swagger.Swagger2SpringBoot;
import com.swagger.api.UsersApiController;
import com.swagger.model.SUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(UsersApiController.class)
@ContextConfiguration(classes = Swagger2SpringBoot.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void findAllUsers() throws Exception {
        // given
        SUser sUser = new SUser();
        sUser.setId(1L);
        sUser.setPassportNumber(12345L);
        sUser.setEmail("test@gmail.com");
        SUser sUser1 = new SUser();
        sUser1.setId(2L);
        sUser.setPassportNumber(123456L);
        sUser1.setEmail("test2@gmail.com");
        List<SUser> userEntities = new ArrayList<>(Arrays.asList(sUser, sUser1));
        Page<SUser> expectedResult = new PageImpl<>(userEntities);

        // mock
        doReturn(expectedResult).when(userService).findAllUsers(0,2);

        // when + then
        this.mockMvc.perform(get("/users").accept("application/json")
                .param("pageNumber","0")
                .param("pageSize","2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].email", is(sUser.getEmail())));
    }

    @Test
    public void findUserById() throws Exception {
        // given
        SUser sUser = new SUser();
        sUser.setId(1L);
        sUser.setPassportNumber(12345L);
        sUser.setEmail("test@gmail.com");

        // mock
        doReturn(sUser).when(userService).findUserByPassportNumber(12345L);

        // when + then
        this.mockMvc.perform(get("/users/{password}", 12345L).accept("application/json")).andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is(sUser.getEmail())));
    }
}
