package com.simple.swag.controller;

import com.simple.swag.enumeration.SimCardStatus;
import com.simple.swag.service.SimCardService;
import com.swagger.Swagger2SpringBoot;
import com.swagger.api.UsersApiController;
import com.swagger.model.SSimCard;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(UsersApiController.class)
@ContextConfiguration(classes = Swagger2SpringBoot.class)
public class SimCardControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SimCardService simCardService;

    @Test
    public void findActivatedSimCardById() throws Exception {
        // given
        SSimCard sSimCard = new SSimCard();
        sSimCard.setId(1L);
        sSimCard.setStatus(SimCardStatus.ACTIVATED.name());

        // mock
        doReturn(sSimCard).when(simCardService).findActivatedSimCardById(1L);

        // when + then
        this.mockMvc.perform(get("/sim-cards/{id}", 1L).accept("application/json")).andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(SimCardStatus.ACTIVATED.name())));
    }

    @Test
    public void findAllActivatedSimCard() throws Exception {
        // given
        SSimCard sSimCard = new SSimCard();
        sSimCard.setId(1L);
        sSimCard.setStatus(SimCardStatus.ACTIVATED.name());
        SSimCard sSimCard1 = new SSimCard();
        sSimCard1.setId(2L);
        sSimCard1.setStatus(SimCardStatus.ACTIVATED.name());
        List<SSimCard> simCardEntities = new ArrayList<>(Arrays.asList(sSimCard, sSimCard1));
        Page<SSimCard> expectedResult = new PageImpl<>(simCardEntities);

        // mock
        doReturn(expectedResult).when(simCardService).findAllActivatedSimCard(0,2);

        // when + then
        this.mockMvc.perform(get("/sim-cards").accept("application/json")
                .param("pageNumber","0")
                .param("pageSize","2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(sSimCard.getId().intValue())));
    }
}
