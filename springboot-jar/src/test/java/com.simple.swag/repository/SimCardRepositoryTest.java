package com.simple.swag.repository;

import com.simple.swag.entity.PackEntity;
import com.simple.swag.entity.SimCardEntity;
import com.simple.swag.enumeration.PackStatus;
import com.simple.swag.enumeration.SimCardStatus;
import com.simple.swag.util.Util;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class SimCardRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private SimCardRepository simCardRepository;

    @Before
    public void setUp(){
        // given
        SimCardEntity simCardEntity = new SimCardEntity();
        simCardEntity.setId(0L);
        simCardEntity.setStatus(SimCardStatus.ACTIVATED.name());
        testEntityManager.persist(simCardEntity);

        SimCardEntity simCardEntity1 = new SimCardEntity();
        simCardEntity1.setId(1L);
        simCardEntity1.setStatus(SimCardStatus.ACTIVATED.name());
        PackEntity packEntity = new PackEntity();
        packEntity.setMinutes(30);
        packEntity.setBytes(10L);
        packEntity.setDateEnd(Util.getCurrentDate());
        packEntity.setStatus(PackStatus.UNAVAILABLE.name());
        packEntity.setSimCardEntity(simCardEntity1);
        packEntity = testEntityManager.persist(packEntity);
        simCardEntity1.setPackEntities(new HashSet<PackEntity>(Arrays.asList(packEntity)));
        testEntityManager.persist(simCardEntity1);

        SimCardEntity simCardEntity2 = new SimCardEntity();
        simCardEntity2.setId(2L);
        simCardEntity2.setStatus(SimCardStatus.DEACTIVATED.name());
        testEntityManager.persist(simCardEntity2);
    }

    @Test
    public void findAllActivatedByOrderByIdDesc(){
        // when
        Pageable pageable = PageRequest.of(0, 2);
        Page<SimCardEntity> result1 = simCardRepository.findAllActivatedByOrderByIdDesc(pageable);
        // then
        assertThat(result1.getTotalElements()).isEqualTo(2);
        assertThat(result1.getTotalPages()).isEqualTo(1);
        assertThat(result1.getContent().get(0).getId()).isEqualTo(1L);
    }

    @Test
    public void findActivatedById(){
        // when
        SimCardEntity result = simCardRepository.findActivatedById(0L);
        // then
        assertThat(result.getId()).isEqualTo(0L);
    }

    @Test(expected = JpaObjectRetrievalFailureException.class)
    public void deleteByStatus(){
        // when
        simCardRepository.deleteByStatus(SimCardStatus.DEACTIVATED.name());
        SimCardEntity result = simCardRepository.getOne(2L);
    }
}
