package com.simple.swag.repository;

import com.simple.swag.entity.UserEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setUp(){
        // given
        UserEntity userEntity = new UserEntity();
        userEntity.setPassportNumber(12345L);
        userEntity.setEmail("test1@gmail.com");
        testEntityManager.persist(userEntity);

        UserEntity userEntity1 = new UserEntity();
        userEntity1.setPassportNumber(123456L);
        userEntity1.setEmail("test1@gmail.com");
        testEntityManager.persist(userEntity1);
    }

    @Test
    public void findAllByOrderByPassportNumberAsc(){
        // when
        Pageable pageable = PageRequest.of(1, 1);
        Page<UserEntity> result = userRepository.findAllByOrderByPassportNumberAsc(pageable);

        // then
        assertThat(result.getTotalElements()).isEqualTo(2);
        assertThat(result.getTotalPages()).isEqualTo(2);
        assertThat(result.getContent().get(0).getPassportNumber()).isEqualTo(123456L);
    }

    @Test
    public void findByPassportNumber(){
        // when
        UserEntity result = userRepository.findByPassportNumber(123456L);

        // then
        assertThat(result.getEmail()).isEqualTo("test1@gmail.com");
    }

    @Test
    public void existsByPassportNumber(){
        // when
        boolean result1 = userRepository.existsByPassportNumber(123456L);
        boolean result2 = userRepository.existsByPassportNumber(1234567L);

        // then
        assertThat(result1).isTrue();
        assertThat(result2).isFalse();
    }
}
