package com.simple.swag.repository;

import com.simple.swag.entity.ExistPackEntity;
import com.simple.swag.entity.PackEntity;
import com.simple.swag.entity.SimCardEntity;
import com.simple.swag.enumeration.PackStatus;
import com.simple.swag.enumeration.SimCardStatus;
import com.simple.swag.util.Util;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static com.simple.swag.constant.Constants.TIME_DATE_PATTERN;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
public class PackRepositoryTest {

    static Long id;

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private PackRepository packRepository;

    @Autowired
    private ExistPackRepository existPackRepository;

    @Before
    public void setUp(){
        // given
        SimCardEntity simCardEntity1 = new SimCardEntity();
        simCardEntity1.setId(1L);
        simCardEntity1.setStatus(SimCardStatus.ACTIVATED.name());
        PackEntity packEntity = new PackEntity();
        packEntity.setMinutes(10);
        packEntity.setBytes(1024L);
        LocalDateTime dt = LocalDateTime.now().plusDays(1); //plus days
        Long dateEnd = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt));
        packEntity.setDateEnd(dateEnd);
        packEntity.setStatus(PackStatus.AVAILABLE.name());
        packEntity.setSimCardEntity(simCardEntity1);
        PackEntity packEntity1 = new PackEntity();
        packEntity1.setMinutes(300);
        packEntity1.setBytes(1000L);
        LocalDateTime dt1 = LocalDateTime.now().minusSeconds(1); //minus 1 sec
        Long dateEnd1 = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt1));
        packEntity1.setDateEnd(dateEnd1);
        packEntity1.setStatus(PackStatus.AVAILABLE.name());
        packEntity1.setSimCardEntity(simCardEntity1);
        PackEntity packEntity2 = new PackEntity();
        packEntity2.setMinutes(30);
        packEntity2.setBytes(10L);
        packEntity2.setDateEnd(Util.getCurrentDate());
        packEntity2.setStatus(PackStatus.UNAVAILABLE.name());
        packEntity2.setSimCardEntity(simCardEntity1);
        PackEntity packEntity3 = new PackEntity();
        packEntity3.setMinutes(30);
        packEntity3.setBytes(1024L);
        LocalDateTime dt3 = LocalDateTime.now().plusDays(2); //plus days
        Long dateEnd3 = Long.parseLong(DateTimeFormatter.ofPattern(TIME_DATE_PATTERN).format(dt3));
        packEntity3.setDateEnd(dateEnd3);
        packEntity3.setStatus(PackStatus.AVAILABLE.name());
        packEntity3.setSimCardEntity(simCardEntity1);
        packEntity = testEntityManager.persist(packEntity);
        id = packEntity.getId();
        packEntity2 = testEntityManager.persist(packEntity2);
        packEntity1 = testEntityManager.persist(packEntity1);
        packEntity3 = testEntityManager.persist(packEntity3);
        simCardEntity1.setPackEntities(new HashSet<PackEntity>(Arrays.asList(packEntity, packEntity1, packEntity2, packEntity3)));
        testEntityManager.persist(simCardEntity1);

        ExistPackEntity existPackEntity = new ExistPackEntity();
        existPackEntity.setMinutes(55);
        existPackEntity.setBytes(2048L);
        existPackEntity.setDayLimit(2);
        testEntityManager.persist(existPackEntity);
    }

//    PackRepository test
    @Test
    public void findAllAvailableBySimCardEntityId(){
        final Long currentDate = Util.getCurrentDate();
        Pageable pageable = PageRequest.of(0, 2);
        // when
        Page<PackEntity> result = packRepository.findAllAvailableBySimCardIdOrderByDateEndAsc(1L, currentDate, pageable);
        // then
        assertThat(result.getTotalElements()).isEqualTo(2);
        assertThat(result.getTotalPages()).isEqualTo(1);
        assertThat(result.getContent().get(0).getMinutes()).isEqualTo(10);
        assertThat(result.getContent().get(0).getBytes()).isEqualTo(1024L);
    }

    @Test
    public void findByIdAndStatus(){
        final Long currentDate = Util.getCurrentDate();
        // when
        PackEntity result = packRepository.findAvailableByIdAndStatus(id, currentDate);
        // then
        assertThat(result.getId()).isEqualTo(id);
    }

    @Test
    public void findAllUnavailableOrderByIdDesc(){
        final Long currentDate = Util.getCurrentDate();
        // when
        List<PackEntity> result = packRepository.findAllUnavailableOrderByIdDesc(currentDate);
        // then
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getMinutes()).isEqualTo(300);
        assertThat(result.get(0).getBytes()).isEqualTo(1000L);
    }

    @Test
    public void deleteByStatus(){
        final Long currentDate = Util.getCurrentDate();
        // when
        packRepository.deleteByStatus(PackStatus.AVAILABLE.name());
        Page<PackEntity> result = packRepository.findAllAvailableBySimCardIdOrderByDateEndAsc(1L, currentDate, null);
        // then
        assertThat(result.getTotalElements()).isEqualTo(2L);
    }

//    ExistPackRepository test
    @Test
    public void findAllByOrderByIdDesc(){
        // when
        Pageable pageable = PageRequest.of(0, 2);
        Page<ExistPackEntity> result = existPackRepository.findAllByOrderByIdDesc(pageable);
        // then
        assertThat(result.getTotalElements()).isEqualTo(1);
        assertThat(result.getTotalPages()).isEqualTo(1);
        assertThat(result.getContent().get(0).getId()).isEqualTo(1L);
        assertThat(result.getContent().get(0).getMinutes()).isEqualTo(55);
        assertThat(result.getContent().get(0).getBytes()).isEqualTo(2048L);
    }

    @Test
    public void existsByMinutesAndBytesAndDayLimit(){
        // when
        boolean result = existPackRepository.existsByMinutesAndBytesAndDayLimit(55, 2048L, 2);
        // then
        assertThat(result).isTrue();
    }
}
